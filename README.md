# Beaker metrics

This repo is undergoing a change and only 1 script is currently relevant.

This script is called `get_possible_broken_machines.py` and it will get broken
machines from [Beaker](https://beaker-project.org) instances. The produced list
of machines is used when submitting testing jobs so we avoid infrastructure
failures which we'd encounter by running on these machines.

## Usage

The scripts reads settings from environment variables

* `PG_TEIID_CON_STR`: psql connection string, e.g. `psql -H -h <hostname> -p <port> -d <dbname> --set=sslmode=require`

Given the environment variables are present, an example query looks like

```
python3 get_possible_broken_machines.py \
        --threshold-recipes-run 15 \
        --threshold-broken 0.7 \
        --days 7 \
        --limit 40 > beaker-possible-broken-machines.txt
```

After the call, `beaker-possible-broken-machines.txt` will contain a list of
hostnames of the machines with highest percentage of aborted recipes.
