#!/usr/bin/env python3
# Copyright (c) 2018 - 2020 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Get a list of hosts to avoid for CKI Beaker Jobs"""
import argparse
import logging
import os
import sys

from datetime import date
from datetime import timedelta

from cki_lib.misc import get_env_var_or_raise
from cki_lib.teiid import TeiidConnector


def main():
    """Do everything (parse args, get data from db, create list)."""
    logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s',
                        level=os.getenv('LOG_LEVEL', 'DEBUG'),
                        stream=sys.stderr)
    logging.getLogger("urllib3.connectionpool").setLevel(logging.WARNING)
    parser = argparse.ArgumentParser(
        description='Get possible broken machines'
    )
    parser.add_argument(
        '--threshold-recipes-run',
        help='Minimum recipes run',
        default=15,
        type=int,
    )
    parser.add_argument(
        '--threshold-broken',
        help='Threshold for broken machines',
        default=0.7,
        type=float,
    )
    parser.add_argument(
        '--days',
        help='Days used in the query',
        default=7,
        type=int,
    )
    parser.add_argument(
        '--limit',
        help='Limit results',
        default=50,
        type=int,
    )
    args = parser.parse_args()
    today = date.today()
    x_days_back = str(today - timedelta(days=args.days))
    today = str(today)

    connection_str = get_env_var_or_raise('PG_TEIID_CON_STR')
    metrics_conn = TeiidConnector(connection_str, timeout=None)
    proc = metrics_conn.run()

    query = f'''
    SELECT abrt.fqdn, (abrt.cnt/ran.cnt) FROM
    (
        SELECT recipe_resource.fqdn, count(recipe_resource.fqdn)::float AS cnt
        FROM recipe LEFT JOIN recipe_resource
        ON recipe.id = recipe_resource.recipe_id
        WHERE recipe.status = 'Aborted' AND
        (recipe.start_time Between '{x_days_back}' and '{today}')
        GROUP BY (recipe_resource.fqdn) HAVING count(recipe_resource.fqdn) > 0
    ) AS abrt,
    (
        SELECT recipe_resource.fqdn, count(recipe_resource.fqdn)::float AS cnt
        FROM recipe LEFT JOIN recipe_resource
        ON recipe.id = recipe_resource.recipe_id
        WHERE
        (recipe.start_time Between '{x_days_back}' and '{today}')
        GROUP BY (recipe_resource.fqdn) HAVING count(recipe_resource.fqdn) > 0
    ) AS ran
    WHERE ran.fqdn = abrt.fqdn
    GROUP BY abrt.fqdn, abrt.cnt, ran.cnt
    HAVING abrt.cnt/ran.cnt > {args.threshold_broken} AND
    ran.cnt > {args.threshold_recipes_run}
    LIMIT {args.limit};"""
    '''

    rows, _ = metrics_conn.query(query)

    for fqdn, ratio in rows:
        # Do NOT remove or change this print -- this is the output that
        # will be redirected to the file!
        print(fqdn)
        logging.debug('%s ratio of broken recipes / recipes run %s',
                      fqdn, ratio)

    proc.join(1)
    proc.kill()

    if metrics_conn.connection_issue:
        sys.exit(1)

if __name__ == '__main__':
    main()
